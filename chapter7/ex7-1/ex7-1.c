/* This is a solution to exercise 7.1 in R&K's C
It's a program that's supposed to convert input into upper or lower case, depending
on the name it'll be called from
Marcin Morawski 2018
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

/* This is the main loop of the program, where the magic happens */
int main(int argc, char *argv[])
{
  /* First, we define a variable to store the chars which we'll be working with
  It's an int, because the book says so, I have yet to figure out why. Seemingly,
  a simple char should be enough. I guess it's so that we'll be able to keep larger
  variables in there (maybe for different encodings?)*/
  int c;

  /* ------------------Reading input lines-------------------------------------
  First, we need to figure out, what the user wants us to do. We'll be interpreting the
  command line arguments.
  */

  // We check if the user gave us the correct number of arguments
  if (argc != 1) {
    printf("Usage: change letter to upper (call as upper) or lowecase (call as lower)\n");
    return -1;
  }

  // Then, we try to extract the name with which the program was called
  // In order to do this, we need to get rid of the filepath from argv[0]
  // This is done using strrchr, and searching for the last '/'

  char *calledName = strrchr(argv[0], '/');
  calledName++; // This pointer incrementing is done so that we'll get rid of the '/'

  // Then, we see what the filepath is using strcmp. For each option we do the same thing -
  // read the input chars using getchar, convert them to appropriate case
  // using standard library functions and put it back with putchar.

  if (strcmp(calledName, "upper") == 0) {
    // This is the case when we want everything to be uppercase
    while ((c = getchar()) != EOF)
      putchar(toupper(c));
  } else if (strcmp(calledName, "lower") == 0) {
    // Now we'd like to make everything lowercase
    while ((c = getchar()) != EOF)
      putchar(tolower(c));
  } else {
    // If it's neither upper or lower, the user made a mistake
    printf("Usage: change letter to upper (call as upper) or lowecase (call as lower)\n");
    return -1;
  }

// And that's it.
  return 0;
}
