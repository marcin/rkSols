/* This is a solution to exercise 7.6 in R&K's C
It's a program which prints the first line where two files differ
M. Morawski 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX_LINE_LENGTH 200

/* This is the main() funciton where the magic happens */
int main(int argc, char *argv[])
{
    // Some initial housekeeping
    char *prog = argv[0]; // Get program filename for error reporting etc.
    FILE *fp1, *fp2; // Create pointers to the files we'll be using
    char linebuf1[MAX_LINE_LENGTH], linebuf2[MAX_LINE_LENGTH]; // Buffers for storing the lines that differ
    int lineCount; // This variable will cointain the line number of the line that differs


    // Function prototypes
    int fileCompare(char *line, char *line2, FILE *fp1, FILE *fp2);

    /* First, we need to open the two files we'll be comparing. Let's do some  error checking too */

    // Open the two files
    // Then, try to open the files. Begin by checking if the user supplied the correct number of arguments
    if (argc != 3) {
        fprintf(stderr, "%s: incorrect number of input arguments.\nUsage - linediff file1 file2", prog);
        exit(1);
    }
    /* Now use fopen to open the files. If this fails - notify the user */
    // File 1:
    if ((fp1 = fopen(*argv++, "r")) == NULL) {
        fprintf(stderr, "%s: can't open %s\n", prog, *argv);
        exit(2);
    }
    // File 2:
    if ((fp2 = fopen(*argv++, "r")) == NULL) {
        fprintf(stderr, "%s: can't open %s\n", prog, *argv);
        exit(2);
    }

    if ((lineCount = fileCompare(linebuf1, linebuf2, fp1, fp2)) != 0){
        printf("The files differ at line %d\n The first file says: %s\n The second file says: %s\n", lineCount, linebuf1, linebuf2);
    }
    else
        printf("The files are identical");

    return 0;
}

// fileCompare - This function actually does the comparing. Returns the number of the line where the two files differ
// linebuf1, linebuf2 - pointers to buffers where the first differing lines will be stored
// fp1, fp1 - pointers to files
int fileCompare(char *linebuf1, char *linebuf2, FILE *fp1, FILE *fp2) {
    // First, reserve some storage:
    int lineCount = 0; // This is where we'll be keeping track of how many lines have been analysed
    char line1[MAX_LINE_LENGTH], line2[MAX_LINE_LENGTH]; // Buffer for storing lines from the files

    // The function just reads lines from two files, compares them, and, if they're the same, increments the line counter. The loop stops if any of the files ends
    while (fgets(line1, MAX_LINE_LENGTH, fp1) &&
    fgets(line2, MAX_LINE_LENGTH, fp2)) {
        if ((strcmp(line1, line2)) != 0)
            break;
        lineCount++;
    }
    // There's a chance that the files are identical. In that case, this function will return 0, and 'garbage' in linebufs
    if ((strcmp(line1,line2)) == 0)
        return 0;
    else {
        linebuf1 = line1;
        linebuf2 = line2;
        return lineCount;
    }
}
