/* This is a solution to exercise 7.8 in R&K's C
It's a program which prints files on pages, starting each new file on a new page.
M. Morawski 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define MAX_LINE_LENGTH 4096
#define PAGE_LENGTH 5

// Globals
static int pageCount = 0; // THis one keeps track of the page number


// Function prototypes
int file_print(FILE *, char *filename);
void new_page(int pageno);

/* This is the main() funciton where the magic happens */
int main(int argc, char *argv[])
{
    // Some initial housekeeping
    char *prog = argv[0]; // Get program filename for error reporting etc.
    FILE *fp; // Pointer to the currently open file

    /* This is the main loop, this program acts more or less like cat */

    /* If no arguments were passed, print standard input to standard output*/
    if (argc == 1)
    file_print(stdin, "stdin");
    else {
        while (--argc > 0) { // while we still have files to open
            if ((fp = fopen(*++argv, "r")) == NULL) { // try to open a file, if it's not possible, print it to stderr
            fprintf(stderr, "%s: can't open %s\n", prog, *argv);
            exit(1);
        }
        else { // if the file did open, start copying lines
            new_page(NULL);
            file_print(fp, *argv);
            fclose(fp);
        }
    }
}
exit(0);
}

// file_print - This function actually does the printing. Returns current line no.
// fp - pointer to the file which is to be printed
int file_print(FILE * fp, char *filename) {
    // First, reserve some storage:
    static int lineCount = 0; // This is where we'll be keeping track of how many lines have been processed
    char line[MAX_LINE_LENGTH]; // This one will store chars in a line

    // first, print a filename and reset pagecount
    fprintf(stdout, "%s\n", filename);
    pageCount = 1;
    // Just read a line from the file and push it onto stdout. If the number of line exceeds page length, start a new page
    while (fgets(line, MAX_LINE_LENGTH, fp) != NULL) {
        fprintf(stdout, line);
        lineCount++;

        if (lineCount > PAGE_LENGTH){
            new_page(pageCount);
            lineCount = 0;
            pageCount++;
        }

    }
    return lineCount;
}

// new_page - this function implements the new page functionality
void new_page(int pagecount) {
    // for now, it just prints three blank lines and the page count

    if (pagecount)
    fprintf(stdout, "Page %d\n", pagecount);
    fprintf(stdout, "\n\n\n");
}
