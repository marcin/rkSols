/* This is a solution to exercise 7.2 in R&K's C
It's a program that's supposed to print arbitrary input in a sensible manner.
M. Morawski 2018
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define LINE_LENGTH 80

/* This is the main() funciton where the magic happens */
int main(int argc, char *argv[])
{
    /* We implement the minimum required funcitonality - the program will only break long lines and display unprintable chars in hexadecimal form */

    // ---------------------Variable definitions--------------------------------

    // In order to break long lines, we need to define a variable to count line length
    int line_count;

    // We also need a variable to store the acquired characters
    int c;

    /* Now comes the main loop - we'll be receiving chars... */
    while ((c=getchar()) != EOF) {
        // incrementing the char counter
        line_count++;
        // checking if they are a newline character - if yes, we need to restart the counter character in the line...
        if (c == '\n')
            line_count = 0;
        // checking if the character is printable
        else if (isprint(c)) {
            // if it is, just push it out
            putchar(c);
        }
        else {
            // if it is not, convert it to hexadecimal and printf
            printf("0x%x", c);
        }

        // Then, if we've exceeded the allowed line length, break up the line. This implementation does not take account of individual words - it'll just insert a newline character in the middle of a phrase
        if (line_count >= LINE_LENGTH){
            printf("%c", '\n');
            line_count = 0;
        }
    }

    return 0;
}
