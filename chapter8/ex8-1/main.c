/* This is a solution to exercise 8.1 in R&K's C
It's a simplified cat written with system calls
M. Morawski 2018
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <fcntl.h>
#include <sys/syscall.h>
#include <unistd.h>

#define BLOCKSIZE 4096

// Globals

// Function prototypes
int cat_stdlib(int argc, char *argv[]);
int cat_syscalls(int argc, char *argv[]);

/* This is the main() funciton where the magic happens */
int main(int argc, char *argv[])
{
    // Let's reserve some space for variables. We'll be measuring the running time, so we need a number for that:
    double running_time;
    clock_t start;
    clock_t end;

    /* first, we call the standard version of cat and measure its running time*/
    start = clock();
    cat_stdlib(argc, &*argv);
    end = clock();

    running_time = (double) (end - start) / CLOCKS_PER_SEC;
    printf("standard cat: %f s\n", running_time);

    /* then, we do the same with the syscalls version */
    start = clock();
    cat_stdlib(argc, &*argv);
    end = clock();

    running_time = (double) (end - start) / CLOCKS_PER_SEC;
    printf("syscalls cat: %f s\n", running_time);



    exit(0);
}

/* this is the standard version of cat from R&K */
int cat_stdlib(int argc, char *argv[])
 {
    FILE *fp;
    void filecopy_stdlib(FILE *, FILE *);
    char *prog = argv[0];

    if (argc == 1) // no arguments, copy standard input
        filecopy_stdlib(stdin, stdout);
    else
        while (--argc > 0)
            if ((fp = fopen(*++argv, "r")) == NULL) {
                fprintf(stderr, "%s: can't open %s\n",
                    prog, *argv);
                exit(1);
            } else {
                filecopy_stdlib(fp, stdout);
                fclose(fp);
            }

    if (ferror(stdout)) {
        fprintf(stderr, "%s: error writing stdout\n", prog);
        exit(2);
    }

    return 1;
}

void filecopy_stdlib(FILE *ifp, FILE *ofp)
{
    int c;

    while ((c = getc(ifp)) != EOF)
        putc(c, ofp);
}


/* this is the standard version of cat from R&K */
int cat_syscalls(int argc, char *argv[])
 {
    int fd;
    void filecopy_syscalls(int, int);
    char *prog = argv[0];

    if (argc == 1) // no arguments, copy standard input to stdout
        filecopy_syscalls(0, 1);
    else
        while (--argc > 0)
            if ((fd = open(*++argv, O_RDONLY, 0)) == -1) {
                fprintf(stderr, "%s: can't open %s\n",
                    prog, *argv);
            } else {
                filecopy_syscalls(fd, 1);
                close(fd);
            }

    if (ferror(stdout)) {
        fprintf(stderr, "%s: error writing stdout\n", prog);
        exit(2);
    }

    return 1;
}

/* filecopy_syscalls - copy chars from imput file (ifd) to output (ofd) */
void filecopy_syscalls(int ifd, int ofd)
{
    int c;

    while (c != EOF){
        write(ofd, &c, 1);
        read(ifd, &c, 1);
    }
}
